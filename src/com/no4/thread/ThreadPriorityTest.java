package com.no4.thread;

/**
 * @author Admin
 */
public class ThreadPriorityTest implements Runnable{
    private int i=0;
    @Override
    public void run() {
        for (i = 0; i <=20 ; i++) {
            System.out.println(Thread.currentThread().getName()+":"+i);
        }
    }

    public static void main(String[] args) {
        System.out.println(Thread.currentThread().getPriority());
        ThreadPriorityTest tt = new ThreadPriorityTest();
        Thread t1 = new Thread(tt,"线程1");
        Thread t2 = new Thread(tt,"线程2");
        //设置线程2的优先级为最高
        t2.setPriority(Thread.MAX_PRIORITY);
        t1.start();
        t2.start();
    }
}
