package com.no4.thread;

import java.util.Date;

/**
 * @author Admin
 */
public class SleepTest {
    public static void main(String[] args) {
        for (int i = 0; i <10 ; i++) {
            System.out.println(Thread.currentThread().getName()+":"+i+"--"+new Date());
            if(i==5){
                try {
                    Thread.sleep(2000);
                    System.out.println(Thread.currentThread().getName()+":"+i+"--"+new Date());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
