package com.no4.thread;

/**
 * @author Admin
 */
public class JoinTest implements Runnable{
    /**
     * 宿主线程
     */
    public Thread container;

    public JoinTest(Thread container) {
        this.container = container;
        System.out.println("Thread Created...");
    }
    @Override
    public void run() {
        for (int i = 0; i <=20; i++) {
            System.out.println(container.getName()+":"+container.getState());
            System.out.println(Thread.currentThread().getName()+":"+i);
            System.out.println(Thread.currentThread().getName()+":"+Thread.currentThread().getState());
        }
    }
    public static void main(String[] args) {

        Thread t1 = new Thread(new JoinTest(Thread.currentThread()));
        t1.start();
        for (int b = 0; b <=50; b++) {
            System.out.println(Thread.currentThread().getName()+":"+b);
            if(b==30){
                try {
                    t1.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
