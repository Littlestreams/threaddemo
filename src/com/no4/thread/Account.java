package com.no4.thread;

import sun.util.resources.cldr.ka.LocaleNames_ka;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Admin
 */
public class Account {
    /**
     *账户编号
     */
    private String accountNo;
    /**
     * 账户余额
     */
    private double balance;
    /**
     *账户中是否有存款的标识
     */
    private boolean flag = false;


    public Account(String accountNo, double balance) {
        this.accountNo = accountNo;
        this.balance = balance;
    }

    /**
     * 取钱
     */
    public synchronized void draw(double drawMoney) {
        try {
            if (!flag) {
                wait();
            } else {
                //开始取钱
                System.out.println(Thread.currentThread().getName() + "取钱：" + drawMoney);
                balance -= drawMoney;
                System.out.println("账户余额：" + balance);
                flag = false;
                notify();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 存钱
     */
    public synchronized void deposit(double depositMoney) {
        try {
            if (flag) {
                wait();
            } else {
                //开始存钱
                System.out.println(Thread.currentThread().getName() + "存钱:" + depositMoney);
                balance += depositMoney;
                System.out.println("账户余额：" + balance);
                flag = true;
                notify();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
        Account account =new Account("NO001",0.0);
        //取钱线程
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i <10 ; i++) {
                    account.draw(800);
                }
            }
        },"取钱者甲").start();
        //存钱线程
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int a = 0; a <10 ; a++) {
                    account.deposit(800);
                }
            }
        },"存钱者乙").start();
    }
}