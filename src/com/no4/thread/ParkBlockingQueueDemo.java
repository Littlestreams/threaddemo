package com.no4.thread;

/**
 * @description:
 * @author: Admin
 * @time: 2020/12/23 15:27
 */
public class ParkBlockingQueueDemo {
    /**
     * 空车位数量
     */
    private int count = 3;

    /**
     * 车辆进入
     */
    public synchronized void carInto(int i) {
        try {
            while (count == 0) {
                System.out.println("当前没有空车位,车辆暂时无法进入");
                wait();
            }
                System.out.println("车辆入库" + i + "辆");
                count -= i;
                System.out.println("此时剩下的空车位的数量为：" + count);
                //当有空车位的时候，唤醒线程
                notifyAll();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 车辆离开
     */
    public synchronized void carOut(int i) {
        try {
            while (count == 3) {
                System.out.println("空车位充足,车辆可以入库");
                wait();//车辆离开的线程等待，入库线程开始
           }
                System.out.println("车辆离开" + i + "辆");
                count += i;
                System.out.println("此时剩下的空车位的数量为：" + count);
                //当有空车位的时候，唤醒线程
                notifyAll();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        ParkBlockingQueueDemo pqd =new ParkBlockingQueueDemo();
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 1; i <4; i++) {
                 pqd.carOut(i);
                }
            }
        },"outer").start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 1; i <4; i++) {
                    pqd.carInto(i);
                }
            }
        },"inter").start();
    }
}