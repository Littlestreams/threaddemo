package com.no4.thread;

/**
 * @description: 线程打印
 * @author: Admin
 * @time: 2020/12/16 16:44
 */
public class ThreadPrintTest {
    static boolean flag = false;
    private static Object objLock1 = new Object();

    public static void main(String[] args) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (objLock1) {
                    int i=1;
                    try {
                       while(i<=52) {
                           if(!flag) {
                               System.out.print(i++);
                               if (i % 2 == 1) {
                                   flag = true;
                                   objLock1.notifyAll();
                               }
                           }else {
                                objLock1.wait();
                            }
                        }

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (objLock1) {
                    try {
                        for(int x=1;x<=26;x++) {
                            if (flag) {
                                x=x-1;
                                System.out.print((char)('A'+x));
                                    flag = false;
                                    objLock1.notifyAll();
                            } else {
                                objLock1.wait();
                            }
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        }).start();
    }
}
