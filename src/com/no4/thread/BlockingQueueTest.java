package com.no4.thread;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * @description: 阻塞队列的功能和用法
 * @author: Admin
 * @time: 2020/12/23 10:09
 */
public class BlockingQueueTest {
    public static void main(String[] args)  {
        BlockingQueue<String> bq = new ArrayBlockingQueue<String>(2);
        bq.add("java1");
        bq.add("java2");
        //bq.add("java3");
       // bq.put("java4");
        System.out.println(bq.offer("java5"));
        System.out.println(bq);
    }
}
