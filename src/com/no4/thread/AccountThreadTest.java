package com.no4.thread;

/**
 * @author Admin
 */
public class AccountThreadTest{
    public static void main(String[] args) {
        Account account =new Account("NO001",0.0);
        //取钱线程
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i <10 ; i++) {
                    account.draw(800);
                }
            }
        },"取钱者甲").start();
        //存钱线程
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i <10 ; i++) {
                    account.deposit(800);
                }
            }
        },"存钱者乙").start();
    }
}
