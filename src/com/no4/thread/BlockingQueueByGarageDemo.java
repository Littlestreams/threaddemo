package com.no4.thread;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
/**
 * @description:
 * @author: Admin
 * @time: 2020/12/23 14:00
 */
public class BlockingQueueByGarageDemo {
    public static void main(String[] args) throws InterruptedException {
        BlockingQueue<Boolean> bq = new ArrayBlockingQueue<Boolean>(3);
        new GarageProducer(bq,"甲").start();
        new GarageProducer(bq,"乙").start();
        new GarageProducer(bq,"丙").start();
        new GarageProducer(bq,"丁").start();
        Thread.sleep(1000);
        new CarConsumer(bq,"乙").start();

    }
}
class GarageProducer extends Thread{
    private BlockingQueue  bq;
    private String name;
    public GarageProducer(BlockingQueue<Boolean> bq,String name) {
        super();
        this.bq = bq;
        this.name = name;
    }

    @Override
        public void run() {
            try {
                bq.put(true);
                System.out.println(this.name+":停车入库");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    class CarConsumer extends Thread{
        private BlockingQueue  bq;
        private String name;
        public CarConsumer(BlockingQueue<Boolean> bq,String name) {
            super();
            this.bq = bq;
            this.name = name;
        }


        @Override
        public void run() {
            try {
                System.out.println(this.name+":开车离开");
                bq.take();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
