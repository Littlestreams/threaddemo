package com.no4.thread;

/**
 * 死锁问题演示
 * @author Admin
 */
public class DeadLockTest implements  Runnable{
    boolean flag =true;
    /**
     *资源1
     */
    private static Object resource1 = new Object();
    /**
     *资源2
     */
    private static Object resource2 = new Object();

    @Override
    public void run() {
        if (flag){
        //锁定资源1
       synchronized (resource1){
           System.out.println(Thread.currentThread().getName()+":锁定资源1，等待资源2...");
           try {
               Thread.sleep(1000);
           } catch (InterruptedException e) {
               e.printStackTrace();
           }
           synchronized (resource2){
               System.out.println(Thread.currentThread().getName()+":得到资源2...");
           }
       }
       }else{
            synchronized (resource2){
                System.out.println(Thread.currentThread().getName()+":锁定资源2，等待资源1...");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (resource1){
                    System.out.println(Thread.currentThread().getName()+":得到资源1...");
                }
            }
        }
    }

    public static void main(String[] args) {
        DeadLockTest dt1 =new DeadLockTest();
        DeadLockTest dt2 =new DeadLockTest();
        dt2.flag=false;
        Thread t1 =new Thread(dt1);
        Thread t2 =new Thread(dt2);
        t1.start();
        t2.start();
    }
}
