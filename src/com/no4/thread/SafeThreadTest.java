package com.no4.thread;

/**
 * @author Admin
 */
public class SafeThreadTest implements  Runnable{
        private int tickets =0;
        @Override
        public void run() {
            boolean flag =true;
            while(flag){
                //售票
                flag =sell();
            }
        }

        public  boolean sell() {//售票结果返回值表示有无票
            boolean sellFlag =true;
            synchronized ((Object) tickets){
            if(tickets<100){
                //更改票数
                tickets =tickets+1;
                System.out.println(Thread.currentThread().getName()+"售卖出第"+tickets+"张票了");
            }else{
                sellFlag =false;
            }
            }
            try {
                //为了增加出错的机会
                Thread.sleep(15);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return sellFlag;
        }

        public static void main(String[] args) {
            //要多线程运行的售票系统
            com.no4.thread.UnsafeThreadTest unt = new com.no4.thread.UnsafeThreadTest();
            //售票点
            new Thread(unt,"售票处1").start();
            new Thread(unt,"售票处2").start();
            new Thread(unt,"售票处3").start();

        }
}
