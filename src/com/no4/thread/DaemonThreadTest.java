package com.no4.thread;

/**
 * @description:
 * @author: Admin
 * @time: 2020/12/24 16:15
 */
public class DaemonThreadTest extends Thread{
    @Override
    public void run() {
        for (int i = 0; i <20 ; i++) {
            System.out.println(this.getName()+":"+i);
        }
    }

    public static void main(String[] args) {
        DaemonThreadTest dt = new DaemonThreadTest();
        //将此线程设置成后台线程
        dt.setDaemon(true);
        //启动后台线程
        dt.start();
        for (int i = 0; i <15; i++) {
            System.out.println(Thread.currentThread().getName()+":"+i);
        }
        System.out.println(dt.isDaemon());
    }
}
