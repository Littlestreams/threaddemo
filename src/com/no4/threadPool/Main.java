//package com.no4.threadPool;
//
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.apache.logging.log4j.core.LoggerContext;
//import org.zzc.config.PropertyLoader;
//import org.zzc.pojo.Job;
//import org.zzc.utils.JobThread;
//import org.zzc.utils.JobUtil;
//
//import java.io.File;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//
///**
// * @author: joker
// * @date: 2020/12/25 11:00
// * @description: 主函数调用接口
// * 仿照测试平台调用步骤, 入参 excel 返回值 加在最后一行
// */
//public class Main {
//
//    static {
//        initLog(Main.class.getName());
//    }
//
//    private static final Logger LOGGER = LogManager.getLogger(Main.class);
//
//    /**
//     * 请求url 从配置文件获取
//     */
//    private static final String URL = PropertyLoader.INSTANCE.getProperty("url");
//
//    /**
//     * args[0] path 入参文件路径
//     * args[1] path 生成文件路径
//     *
//     * @param args
//     */
//    public static void main(String[] args) {
//        String inputPath = args[0];
//        String outputPath = args[1];
//        try {
//            String time = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
//            Job job = new Job(inputPath, outputPath, URL, time);
//            LOGGER.info("[ add job: {} ]", job);
//            JobUtil.getExecutorService().submit(new JobThread());
//            JobUtil.add(job);
//        } catch (Exception e) {
//            LOGGER.error("MainException : " + e.getMessage());
//            e.printStackTrace();
//        } finally {
//            LOGGER.info("Main end !");
//            JobUtil.stopExecutorService();
//        }
//
//    }
//
//    /**
//     * 指定log4j2.xml文件路径，使用jar外的文件方便配置
//     *
//     * @param mainClass
//     */
//    public static void initLog(String mainClass) {
//        String path = new Object() {
//            public String getPath(String cls) {
//                try {
//                    return Class.forName(cls).getProtectionDomain().getCodeSource().getLocation().getPath();
//                } catch (ClassNotFoundException e) {
//                    e.printStackTrace();
//                }
//                return "";
//            }
//        }.getPath(mainClass);
//        String parent = new File(path).getParent();
//        File file = new File(parent + "/log4j2.xml");
//        // 改变系统参数
//        System.setProperty("log4j.configurationFile", file.toURI().toString());
//        // 重新初始化Log4j2的配置上下文
//        LoggerContext context = (LoggerContext) LogManager.getContext(false);
//        context.reconfigure();
//    }
//
//}
