//package com.no4.threadPool;
//
//import com.jxcell.View;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.zzc.jxcell.DataHelper;
//import org.zzc.pojo.Device;
//import org.zzc.pojo.Job;
//import org.zzc.pojo.QueryData;
//
//import java.io.File;
//
///**
// * @author: joker
// * @date: 2020/12/30 17:34
// * @description:
// */
//public class JobThread implements Runnable {
//
//    private static final Logger LOGGER = LogManager.getLogger(JobThread.class);
//
//    private static final int STARTLINE = 5;
//    private static final int COLUMNS = 50;
//
//    @Override
//    public void run() {
//        System.out.println("into run");
//        while (true) {
//            System.out.println("run foreach");
//            try {
//                Job job = JobUtil.get();
//                LOGGER.info("[run job: {} ]", job);
//
//                // 1. 读取入参文件 并实例化queryData
//                File file = new File(job.getInputPath());
//                if (!file.getParentFile().exists()) {
//                    file.getParentFile().mkdirs();
//                }
//
//                View viewOut = new View();
//                viewOut.setNumSheets(1);
//                viewOut.setSheetName(0, "result");
//                viewOut.setSheet(0);
//
//                View viewIn = new View();
//                DataHelper.readSrc(viewIn, job.getInputPath());
//                viewIn.setSheet(0);
//
//                // 添加数据
//                for (int i = 0; i < STARTLINE; i++) {
//                    for (int j = 0; j < COLUMNS; j++) {
//                        viewOut.setText(i, j, viewIn.getText(i, j).trim());
//                    }
//                }
//
//                // 总行数
//                int rows = viewIn.getLastRow() + 1;
//
//                for (int i = STARTLINE; i < rows; i++) {
//                    if ((i - STARTLINE) % 10 == 0) {
//                        LOGGER.info("loop: {}, {} ", job.getInputPath().substring(job.getInputPath().lastIndexOf("/") + 1), (i - STARTLINE));
//                    }
//
//                    String encrypt_type = viewIn.getText(i, 1).trim();
//                    String loan_type = viewIn.getText(i, 2).trim();
//                    String endTime = viewIn.getText(i, 6).trim();
//                    String name = viewIn.getText(i, 7).trim();
//                    String pid = viewIn.getText(i, 8).trim();
//                    String mobile = viewIn.getText(i, 9).trim();
//                    String home_address = viewIn.getText(i, 10).trim();
//                    String home_phone = viewIn.getText(i, 11).trim();
//                    String work_name = viewIn.getText(i, 12).trim();
//                    String work_address = viewIn.getText(i, 13).trim();
//                    String work_phone = viewIn.getText(i, 14).trim();
//                    String address = viewIn.getText(i, 15).trim();
//                    String apply_at = "";
//                    String startTime = "";
//                    String pidSha256 = null;
//                    String mobileSha256 = null;
//                    String pidMd5 = null;
//                    String mobileMd5 = null;
//                    String pidSm3 = null;
//                    String mobileSm3 = null;
//
//                    // device
//                    String device_type = viewIn.getText(i, 20).trim();
//                    String imei = viewIn.getText(i, 21).trim();
//                    String keychain_based_uuid = viewIn.getText(i, 22).trim();
//                    String idfa = viewIn.getText(i, 23).trim();
//                    String idfv = viewIn.getText(i, 24).trim();
//                    String mac = viewIn.getText(i, 25).trim();
//                    String ip_address = viewIn.getText(i, 26).trim();
//                    String gps_longitude = viewIn.getText(i, 27).trim();
//                    String gps_latitude = viewIn.getText(i, 28).trim();
//                    String os_name = viewIn.getText(i, 29).trim();
//                    String os_version = viewIn.getText(i, 30).trim();
//                    String serial_number = viewIn.getText(i, 31).trim();
//                    String hd_serial_number = viewIn.getText(i, 32).trim();
//                    String browser_name = viewIn.getText(i, 33).trim();
//                    String browser_version = viewIn.getText(i, 34).trim();
//
//                    Device device = new Device();
//                    device.setDevice_type(device_type);
//                    device.setSerial_number(serial_number);
//                    device.setHd_serial_number(hd_serial_number);
//                    device.setImei(imei);
//                    device.setKeychain_uuid(keychain_based_uuid);
//                    device.setIdfa(idfa);
//                    device.setIdfv(idfv);
//                    device.setMac(mac);
//                    device.setOs_name(os_name);
//                    device.setOs_version(os_version);
//                    device.setBrowser_version(browser_version);
//                    device.setBrowser_name(browser_name);
//                    device.setIp_address(ip_address);
//                    device.setGps_latitude(gps_latitude);
//                    device.setGps_longitude(gps_longitude);
//
//                    switch (encrypt_type) {
//                        case "1":
//                            pidSha256 = pid;
//                            mobileSha256 = mobile;
//                            pid = null;
//                            mobile = null;
//                            break;
//                        case "2":
//                            pidMd5 = pid;
//                            mobileMd5 = mobile;
//                            pid = null;
//                            mobile = null;
//                            break;
//                        case "3":
//                            pidSm3 = pid;
//                            mobileSm3 = mobile;
//                            pid = null;
//                            mobile = null;
//                            break;
//                        default:
//                            break;
//                    }
//
//
//                    QueryData data = new QueryData(name, pid, mobile, pidSha256, mobileSha256, pidMd5, mobileMd5, pidSm3, mobileSm3, loan_type, work_name, work_phone, address,
//                            home_phone, work_address, home_address, startTime, endTime, apply_at, device);
//
//                    String result = HttpUtil.httpPost(job.getUrl(), JsonUtil.toJsonString(data, true));
//
//                    for (int j = 0; j < COLUMNS; j++) {
//                        viewOut.setText(i, j, viewIn.getText(i, j).trim());
//                    }
//                    viewOut.setText(i, COLUMNS, result);
//                }
//
//                DataHelper.writeDst(viewOut, job.getOutputPath());
//            } catch (Exception e) {
//                LOGGER.info("[ JobThread error: {} ]", e.getMessage());
//            }
//        }
//    }
//
//}
