//package com.no4.threadPool;
//
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.zzc.pojo.Job;
//
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//import java.util.concurrent.LinkedBlockingQueue;
//
///**
// * @author: joker
// * @date: 2020/12/30 17:32
// * @description:
// */
//public class JobUtil {
//
//    private static final Logger LOGGER = LogManager.getLogger(JobUtil.class);
//
//    private static LinkedBlockingQueue<Job> queue = new LinkedBlockingQueue<Job>();
//
//    private static ExecutorService service = null;
//
//    public static void add(Job job) {
//        try {
//            LOGGER.info("[ add job: {} ]", job);
//            queue.put(job);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static Job get() throws InterruptedException {
//        Job job = queue.take();
//        LOGGER.info("[ remove job: {} ]", job);
//        return job;
//    }
//
//    public synchronized static ExecutorService getExecutorService() {
//        if (service == null) {
//            service = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
//        }
//        return service;
//    }
//
//    public static void stopExecutorService() {
//        LOGGER.info("start to close the thread pool");
//        if (service != null) {
//            service.shutdown();
//        }
//        service = null;
//    }
//
//}
