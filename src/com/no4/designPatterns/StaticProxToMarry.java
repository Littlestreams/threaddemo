package com.no4.designPatterns;

/**
 * @description:  静态代理
 * 1.代理对象和真实对象都要实现同一个接口
 * 2.代理对象要代理真实对象
 * 好处：代理对象可以代替真实对象去做一些真实对象做不了的事
 * 真实对象可以专注做一些自己的事
 * @author: Admin
 * @time: 2021/6/2 14:04
 */
public interface StaticProxToMarry {
    public void  happyMarry();
}

//真实对象类
class HandsomeGuy implements StaticProxToMarry{
    @Override
    public void happyMarry() {
        System.out.println("今天你要嫁给我...");
    }
}
//代理类
class WeddingCompany implements StaticProxToMarry{
    //代理真实对象
    private HandsomeGuy target;

    public WeddingCompany(HandsomeGuy target) {
        this.target = target;
    }

    @Override
    public void happyMarry() {
        before();
        this.target.happyMarry();//真实对象调用
        after();
    }

    public static void main(String[] args) {
        WeddingCompany wc =new WeddingCompany(new HandsomeGuy());
        wc.happyMarry();
    }
    public void before(){
        System.out.println("买束花,买戒指");
    }
    public void after(){
        System.out.println("yes，I do");
    }
}