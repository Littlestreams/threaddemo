package com.no4.demo01;

/**
 * @description: 观察线程状态
 * @author: Admin
 * @time: 2021/6/3 10:08
 */
public class TestThreadState01{
    public static void main(String[] args) throws InterruptedException {
       Thread thread =new Thread(()->{
           for (int i = 0; i <= 5; i++) {//阻塞5s
               try {
                   Thread.sleep(1000);
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }
           }
           System.out.println("线程结束！！");
       });
       //启动前线程状态
        Thread.State state = thread.getState();
        System.out.println("启动前的线程状态:"+state);
        //启动后线程状态
        thread.start();
        state = thread.getState();
        System.out.println("启动后的线程状态:"+state);
        //持续输出线程状态
        while(state != Thread.State.TERMINATED){
            Thread.sleep(100);
            state=thread.getState();//更新线程状态
            System.out.println(state);
        }
    }
}