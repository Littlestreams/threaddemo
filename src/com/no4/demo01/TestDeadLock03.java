package com.no4.demo01;

/**
 * @description: 死锁
 *  当多个线程同时想要获取对方资源不释放，形成了僵持,就会形成死锁
 * @author: Admin
 * @time: 2021/6/3 15:14
 */
public class TestDeadLock03 {
    public static void main(String[] args) {
        Makeup mp1 =new Makeup(0,"灰姑娘");
        Makeup mp2 =new Makeup(1,"白姑娘");

        new Thread(mp1).start();
        new Thread(mp2).start();
    }
}
//口红
class Lipstick{

}
//镜子
class Mirror{

}
//化妆
class Makeup implements Runnable{
    //需要的资源只有一份，所以我们使用static修饰
    static Lipstick li =new Lipstick();
    static Mirror mi =new Mirror();
    private int choice;//选择标志
    private String girlName;

    public Makeup(int choice, String girlName) {
        this.choice = choice;
        this.girlName = girlName;
    }
    @Override
    public void run() {
        //开始化妆
        try {
            makeup();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    //化妆
    private void makeup() throws InterruptedException {
        if (choice==0){
            synchronized (li){
                System.out.println(this.girlName+"获得口红的锁");
                //为了增加出错的机会
                Thread.sleep(1000);
                synchronized (mi){
                    System.out.println(this.girlName+"获得镜子的锁");
                }
            }
        }else{
            synchronized (mi){
                System.out.println(this.girlName+"获得镜子的锁");
                //为了增加出错的机会
                Thread.sleep(2000);
                synchronized (li){
                    System.out.println(this.girlName+"获得口红的锁");
                }
            }
        }
    }
}