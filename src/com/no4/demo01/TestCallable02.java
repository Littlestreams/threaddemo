package com.no4.demo01;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

/**
 * @description:
 * @author: Admin
 * @time: 2021/6/2 16:51
 */
public class TestCallable02 implements Callable<Boolean> {
    private String url;
    private String name;
    public TestCallable02(String url, String name){
        this.url = url;
        this.name = name;
    }
    @Override
    public Boolean call() throws Exception {
        WebDownLoader1 wl = new WebDownLoader1();
        wl.downLoad(url,name);
        System.out.println(name+"图片已经下载完成..");
        return true;
    }

    public static void main(String[] args) {

        FutureTask<Boolean> task1 =new FutureTask<>(new TestCallable02("https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.juimg.com%2Ftuku%2Fyulantu%2F140703%2F330746-140F301555752.jpg&refer=http%3A%2F%2Fimg.juimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1625207046&t=cdd22f7b3e29ea534e166cf850d2d34c","1.jpg"));
        FutureTask<Boolean> task2=new FutureTask<>(new TestCallable02("https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.juimg.com%2Ftuku%2Fyulantu%2F140703%2F330746-140F301555752.jpg&refer=http%3A%2F%2Fimg.juimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1625207046&t=cdd22f7b3e29ea534e166cf850d2d34c","2.jpg"));
        FutureTask<Boolean> task3=new FutureTask<>(new TestCallable02("https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.juimg.com%2Ftuku%2Fyulantu%2F140703%2F330746-140F301555752.jpg&refer=http%3A%2F%2Fimg.juimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1625207046&t=cdd22f7b3e29ea534e166cf850d2d34c","3.jpg"));
        new Thread(task1).start();
        new Thread(task2).start();
        new Thread(task3).start();
    }
}
/**
 * 下载器
 */
class WebDownLoader1 {
    //下载方法
    public void downLoad(String url,String name){
        try {
            FileUtils.copyURLToFile(new URL(url),new File(name));
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("downLoad方法出现异常");
        }
    }
}