package com.no4.demo01;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * @description:  多线程联系，下载网图
 * @author: Admin
 * @time: 2021/6/2 14:04
 */
public class TestThread01 extends Thread{
    private String url;
    private String name;
    public TestThread01(String url,String name){
        this.url = url;
        this.name = name;
    }
   @Override
   public void run(){
       WebDownLoader wl = new WebDownLoader();
       wl.downLoad(url,name);
       System.out.println(name+"图片已经下载完成..");
   }

    public static void main(String[] args) {
        TestThread01 t1 = new TestThread01("https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.juimg.com%2Ftuku%2Fyulantu%2F140703%2F330746-140F301555752.jpg&refer=http%3A%2F%2Fimg.juimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1625207046&t=cdd22f7b3e29ea534e166cf850d2d34c","1.jpg");
        TestThread01 t2 = new TestThread01("https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fsearchfoto.ru%2Fimg%2FxyygpKbDS1y8pTjXRy83VS8rMS9fLSy3RL8mwz0yx9fcM0PPw80hy9Ss3T6pKNPAO8E5xcU0vLAn0SPQNV0vMLbAutzUyNgCzMmwNzSGsomJbQzCjIDnHNgUMwNx8W1OIMNBoQz1DAA.jpg&refer=http%3A%2F%2Fsearchfoto.ru&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1625207046&t=d6b889b5d9877169b295571135a57d9b","2.jpg");
        TestThread01 t3 = new TestThread01("https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fpic1.win4000.com%2Fwallpaper%2F6%2F56d7fe78a9acf.jpg&refer=http%3A%2F%2Fpic1.win4000.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1625207046&t=c0fe05c42671150810fe334c1965708f","3.jpg");
        t1.start();
        t2.start();
        t3.start();
    }
}

/**
 * 下载器
 */
class WebDownLoader{
    //下载方法
   public void downLoad(String url,String name){
       try {
           FileUtils.copyURLToFile(new URL(url),new File(name));
       } catch (IOException e) {
           e.printStackTrace();
           System.out.println("downLoad方法出现异常");
       }
   }
}