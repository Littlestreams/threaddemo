package com.no4.demo01;

/**
 * @description: 测试守护线程
 * 虚拟机必须等待用户线程执行完成
 * 虚拟机不用等待守护线程执行完成
 * 案例：人生不过三万天
 * @author: Admin
 * @time: 2021/6/3 10:43
 */
public class TestDeamon {
    public static void main(String[] args) {
        God god = new God();
        Thread thread = new Thread(god);
        //设置守护线程
        thread.setDaemon(true);//默认是false,表示用户线程
        thread.start();
        //用户线程
        new Thread(new You()).start();
    }
}

//上帝
class God implements Runnable {
    @Override
    public void run() {
        while (true) {
            System.out.println("上帝保佑着你");
        }
    }
}

//你
class You implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 36500; i++) {
            System.out.println("你一生都开开心心的活着..hello world");
        }
        System.out.println("goodbye world!!!");
    }
}