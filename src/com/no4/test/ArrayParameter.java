package com.no4.test;

/**
 * @description: 数组传递
 * @author: Admin
 * @time: 2021/1/11 15:17
 */
public class ArrayParameter {
    int temp;
    /**
     * 使用冒泡法进行排序
     */
    public void sort(int [] b){
       for (int i=0;i<b.length-1;i++){
           for (int j=0;j<b.length-i-1;j++){
               if(b[j]>b[j+1]){
                   int temp = b[j];
                   b[j]=b[j+1];
                   b[j+1]=temp;
               }
           }
       }
    }
    public static void main(String[] args) {
        int [] a ={7,5,4,3};
        System.out.print("排序之前的值：");
        for (int i : a) {
            System.out.print(i+" ");
        }
        ArrayParameter ap = new ArrayParameter();
        ap.sort(a);
        System.out.println("");
        System.out.print("排序之后的值：");
        for (int x : a) {
            System.out.print(x+" ");
        }
    }
}
