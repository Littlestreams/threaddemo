package com.no4.test;

/**
 * @description:  基本数据类型值传递
 * @author: Admin
 * @time: 2021/1/8 10:21
 */
public class ParameterPassValue {
    public static void change(int x){
        x=100;
        System.out.println("方法中的：x="+x);
    }
    public static void main(String[] args) {
        int x =5;
        System.out.println("调用方法之前：x="+x);
        change(x);
        System.out.println("调用方法之后：x="+x);
    }
}
