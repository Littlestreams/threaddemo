package com.no4.test;

/**
 * @description:
 * @author: Admin
 * @time: 2021/1/19 17:43
 */
public class IntanceofTest {
    public static void main(String[] args) {
        //声明h变量时使用Object类，则h的编译类型是Object
        //Object是所有类的父类，但h变量的实际是String
        Object h ="hello";
        //String和Object类存在着继承关系，所以可以使用instanceof判断，返回true
        System.out.println("字符串是否是Object类的实例："+h instanceof Object);
        //返回true
        System.out.println("字符串是否是String类的实例："+h instanceof String);
        //Object是所有类的父类，但h变量的实际是String类型，String和Math没有继承关系，所有下面代码无法编译通过
        //System.out.println("字符串是否是Math类的实例："+h instanceof Math);
        //String实现了Comparable接口，返回true
        System.out.println("字符串是否是Comparable接口的实例："+h instanceof Comparable);
    }
}
