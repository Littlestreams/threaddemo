package com.no4.test;


/**
 * @author Admin
 */
public class SubTest {
    public static void main(String[] args) {
        Animal a = new Dog();//向上转型
        a.eat();
        System.out.println(a.a);
        Dog d = (Dog) a;//向下转型
        d.swim();
        System.out.println(d.a);

    }
}

class Animal{
    int a=10;
    public void eat() {
        System.out.println("吃东西");
    }
}


class Dog extends Animal{
    int a =20;
    @Override
    public void eat() {
        System.out.println("啃骨头");
    }

    public void swim() {
        System.out.println("狗刨");
    }
}