package com.no4.test;

/**
 * @author Admin
 */
public class HelloWorld {
   // private static  boolean flag =false;
    public static void main(String[] args) {
        Print p = new Print();
        new MyThread01(p).start(); //线程1

        new MyThread02(p).start(); //线程2

    }
}

class Print {             //用来确定输出
    public int i = 1;
    public char s = 'A';
    public boolean flag = true;
}

class MyThread01 extends Thread {        //输出数字
    private Print p;

    public MyThread01() {
    };

    public MyThread01(Print p) {
        this.p = p;
    }

    @Override
    public void run() {
        synchronized (p) {
            while (p.i <= 52) {
                if (p.flag) {
                    System.out.print(p.i++);
                    if (p.i % 2 == 1) {
                        p.flag = false;
                        p.notifyAll();
                    }

                } else {
                    try {
                        p.wait();
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

            }

        }
    }

}

class MyThread02 extends Thread {  //输出字母
    private Print p;

    public MyThread02() {
    };

    public MyThread02(Print p) {
        this.p = p;
    }

    @Override
    public void run() {
        synchronized (p) {
            while (p.s <= 'Z') {
                if (!p.flag) {
                    System.out.print(p.s++);
                    p.flag = true;
                    p.notifyAll();
                } else {
                    try {
                        p.wait();
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }

        }
    }
}


