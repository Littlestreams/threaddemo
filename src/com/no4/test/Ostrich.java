package com.no4.test;


class Bird {
    private String name;

    public Bird(String name) {
        this.name = name;
    }

    public  void fly(){
        System.out.println(name+":自由自在的飞翔");
    }
}

public class Ostrich extends Bird{

    public Ostrich(String name) {
        super(name);
    }

    @Override
    public void fly(){
        System.out.println("鸵鸟在自由自在的奔跑");
    }
    public void disFly(){
        //在子类方法中显式通过super调用父类被重写的方法fly()
        super.fly();
        //this调用自己的方法
        this.fly();
    }
    public static void main(String[] args) {
        Ostrich o = new Ostrich("鸵鸟");
        o.disFly();
    }
}
