package com.no4.test;

/**
 * @description:  引用数据类型传递--对象传递
 * @author: Admin
 * @time: 2021/1/11 10:37
 */
public class ParameterPassValue1 {
    private int x;
    public static void change(ParameterPassValue1 p1){
        p1.x=100;
        System.out.println("change方法中："+p1.x);
    }
    public static void main(String[] args) {
        ParameterPassValue1 p1 =new ParameterPassValue1();
        ParameterPassValue1 p2 =p1;
        System.out.println(p1==p2);
        System.out.println(p1.equals(p2));
        p1.x=5;
        System.out.println("调用方法之前："+p1.x);
        change(p1);
        System.out.println("调用方法之后："+p1.x);
    }
}
