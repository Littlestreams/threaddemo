package com.no4.test;

/**
 * @author Admin
 */
public class BeautifulGirl {
    private String style;

    public BeautifulGirl(String style) {
        this.style = style;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

}
