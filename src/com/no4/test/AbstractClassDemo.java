package com.no4.test;

/**
 * @description: 抽象类
 * @author: Admin
 * @time: 2021/1/4 13:53
 */
public abstract class AbstractClassDemo {
   private String name;
   private String memo;
   private int num;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public AbstractClassDemo(String name, String memo, int num) {
        this.name = name;
        this.memo = memo;
        this.num = num;
    }
    public void disPlay(){
        System.out.println("This is " + this.name
                + "," + this.memo+","+this.num);
    }

}
