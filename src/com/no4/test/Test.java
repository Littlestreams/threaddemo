package com.no4.test;

/**
 * @description:
 * @author: Admin
 * @time: 2020/12/18 16:18
 */
/*
 * 定义一个停车场类，两个线程共享停车场，利用同步停车场对象实现停车和出库操作
 * 当停车场满时，wait（）,等待取车操作
 * 当停车场为空时，wait(),等待有车停入
 */
//停车场类
class Park{
    boolean[] park = new boolean[3];
    //state变量定义停车场的剩余车辆
    private int state =3;

    public synchronized void CarIn(int i) {
        try {
            while(state==0) {
                System.out.println("目前空余车位为："+state+"请等待");
                wait();

            }
            System.out.println(i+"车位停车成功");
            state=state-1;
            System.out.println("目前剩余车位为："+state);

            notify();
        }
        catch(InterruptedException e){
            e.printStackTrace();

        }
    }
    public synchronized void CarOut(int i) {
        try {
            while(state==3) {
                //System.out.println("目前空余车位为："+state+"请等待");
                wait();

            }
            System.out.println(i+"车驶出");
            state=state+1;
            System.out.println("目前剩余车位为："+state);

            notify();
        }
        catch(InterruptedException e){
            e.printStackTrace();

        }
    }



}
class CarInThread extends Thread{
    Park park=new Park();
    public CarInThread(Park park) {
        this.park=park;
    }


    @Override
    public void run() {
        super.run();
        for(int i=1;i<5;i++){
            park.CarIn(i);
        }
    }
}
class CarOutThread extends Thread{
    Park park=new Park();
    public CarOutThread(Park park) {
        this.park=park;
    }

    @Override
    public void run() {
        super.run();
        for(int i=1;i<5;i++){
            park.CarOut(i);
        }
    }
}



/**
 * @author Admin
 */
public class Test {
    public static void main(String[] args) {
        Park park = new Park();
        new CarInThread( park).start();
        new CarOutThread(park).start();
    }

}

