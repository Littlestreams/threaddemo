package com.no4.test;

/**
 * @author Admin
 */
public class GoodBoy {
    private BeautifulGirl  beautifulGirl;
    private String likes;

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public BeautifulGirl getBeautifulGirl() {
        return beautifulGirl;
    }

    public void setBeautifulGirl(BeautifulGirl beautifulGirl) {
        this.beautifulGirl = beautifulGirl;
    }

    @Override
    public String toString() {
        return "GoodBoy通过"+getLikes()+"的方式成功追到一个"+getBeautifulGirl().getStyle()+"女孩";
    }
}
